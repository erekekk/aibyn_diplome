from flask import Flask, request, send_file, render_template, render_template_string
from werkzeug.utils import secure_filename
import os
from fhe_operations import encrypt_file, decrypt_file, process_annual_taxes, process_net_income, \
    process_investment_income_percentage, process_depreciation_calculation, process_coverage_ratio, \
    process_solvency_ratio

app = Flask(__name__, template_folder='templates', static_folder='templates')
UPLOAD_FOLDER = 'uploads'
PROCESSED_FOLDER = 'processed'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['PROCESSED_FOLDER'] = PROCESSED_FOLDER
keys_file_path = os.path.join(app.config['UPLOAD_FOLDER'], 'keys.txt')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    if 'file' not in request.files or 'key' not in request.form:
        return 'Missing file or key'
    file = request.files['file']
    key = request.form['key']
    if file.filename == '':
        return 'No selected file'

    filename = file.filename
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    if os.path.exists(file_path):
        os.remove(file_path)

    file.save(file_path)
    try:
        encrypted_file_path = encrypt_file(file_path, key)
        save_key(key, encrypted_file_path)  # Save the key and file mapping
        return send_file(encrypted_file_path, as_attachment=True)
    except Exception as e:
        return str(e)


@app.route('/process', methods=['POST'])
def process():
    if 'file' not in request.files:
        return 'No file part'
    file = request.files['file']
    operation = request.form['operation']
    if file.filename == '':
        return 'No selected file'

    operation = request.form['operation']
    filename = file.filename

    # Check if the filename contains an underscore and ends with .xlsx
    if '_' in filename and filename.endswith('.xlsx'):
        base_name = filename.split('_')[0]  # Get the part before the underscore
        filename = f"{base_name}.xlsx"  # Reconstruct filename with only the base name

    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(file_path)
    print(file_path)

    if operation == 'annual_taxes':
        processed_file_path = process_annual_taxes(file_path)
    elif operation == 'net_income':
        processed_file_path = process_net_income(file_path)
    elif operation == 'investment_income_percentage':
        processed_file_path = process_investment_income_percentage(file_path)
    elif operation == 'depreciation_calculation':
        processed_file_path = process_depreciation_calculation(file_path)
    elif operation == 'coverage_ratio':
        processed_file_path = process_coverage_ratio(file_path)
    elif operation == 'solvency_ratio':
        processed_file_path = process_solvency_ratio(file_path)
    else:
        # Placeholder for other operations
        processed_file_path = file_path

    return send_file(processed_file_path, as_attachment=True)


@app.route('/decrypt', methods=['POST'])
def decrypt():
    if 'file' not in request.files or 'key' not in request.form:
        return render_template_string('<h1>Missing file or key</h1><a href="/">Go Back</a>')
    file = request.files['file']
    key = request.form['key']
    if file.filename == '':
        return render_template_string('<h1>No selected file</h1><a href="/">Go Back</a>')

    filename = file.filename
    file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(file_path)

    try:
        decrypted_file_path = decrypt_file(file_path, key)
        return send_file(decrypted_file_path, as_attachment=True)
    except Exception as e:
        os.remove(file_path)  # Cleanup uploaded file
        return render_template_string('<h1>Invalid key or decryption error</h1><a href="/">Go Back</a>')


def save_key(key, encrypted_file_path):
    # Save the key and the corresponding encrypted file name
    with open(keys_file_path, 'a') as file:
        file.write(f"{key}:{os.path.basename(encrypted_file_path)}\n")


if __name__ == '__main__':
    if not os.path.exists(UPLOAD_FOLDER):
        os.makedirs(UPLOAD_FOLDER)
    if not os.path.exists(PROCESSED_FOLDER):
        os.makedirs(PROCESSED_FOLDER)
    app.run(debug=True)
