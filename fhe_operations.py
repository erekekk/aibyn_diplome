# Placeholder for an FHE library
import hashlib
import os
import random
import string
from base64 import b64encode, b64decode
from hashlib import sha256
import openpyxl
from openpyxl import load_workbook

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
from openpyxl.reader.excel import load_workbook
from openpyxl.workbook import Workbook
from flask import Flask, request, send_file, render_template, render_template_string, logging

app = Flask(__name__, template_folder='templates', static_folder='templates')
UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class PyFHELib:
    @staticmethod
    def encrypt(number):
        # Generate a string of 500 random characters
        random_chars_before = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(500))
        random_chars_after = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(500))
        # Concatenate the parts with the keywords and the number
        return f"{random_chars_before}inc_out{number}out_inc{random_chars_after}"

    @staticmethod
    def decrypt(encrypted_number):
        _, after_inc_out = encrypted_number.split("inc_out", 1)
        # Split the result on "out_inc" to isolate the number
        number_str, _ = after_inc_out.split("out_inc", 1)
        # Convert the extracted number string to an integer
        return int(number_str)

    @staticmethod
    def calculate_twenty_percent(encrypted_number):
        # Simulate performing an operation on encrypted data
        # This is not how FHE works but serves as a placeholder
        number = PyFHELib.decrypt(encrypted_number)
        number = int(number * 0.2)
        return PyFHELib.encrypt(number)


# Actual functions that use the placeholder FHE library
# def encrypt_file(file_path, key):
#     workbook = openpyxl.load_workbook(file_path)
#     sheet = workbook.active
#
#     encrypted_file_path = os.path.splitext(file_path)[0] + "_enc.xlsx"
#
#     # Preparing the encryption key
#     key_bytes = sha256(key.encode()).digest()  # Hash key to ensure it's exactly 32 bytes
#
#     # Encrypt each cell individually
#     for row in sheet.iter_rows():
#         for cell in row:
#             if cell.value is not None:
#                 cell.value = encrypt_value(str(cell.value), key_bytes)
#
#     workbook.save(encrypted_file_path)
#     return encrypted_file_path
def encrypt_file(file_path, key):
    workbook = openpyxl.load_workbook(file_path)
    sheet = workbook.active
    encrypted_file_path = os.path.splitext(file_path)[0] + "_enc.xlsx"
    key_bytes = sha256(key.encode()).digest()  # Hash key

    for row in sheet.iter_rows():
        for cell in row:
            if cell.value is not None:
                # Check if the cell contains a formula
                if isinstance(cell.value, str) and cell.value.startswith('='):
                    # Optionally handle or log the formula
                    continue  # Skip encrypting formulas
                else:
                    # Encrypt non-formula cells
                    cell.value = encrypt_value(str(cell.value), key_bytes)

    workbook.save(encrypted_file_path)
    return encrypted_file_path


def encrypt_value(value, key_bytes):
    iv = os.urandom(16)
    cipher = Cipher(algorithms.AES(key_bytes), modes.CFB(iv), backend=default_backend())
    encryptor = cipher.encryptor()
    encrypted_data = encryptor.update(value.encode()) + encryptor.finalize()
    encrypted_value = b64encode(iv + encrypted_data).decode('utf-8')
    return encrypted_value


def process_annual_taxes(file_path):
    key = get_key_from_file('Годовые налоги_enc.xlsx');
    decrypted_file_path = decrypt_file(file_path, key)  # Ensure this is the correct decryption call
    workbook = load_workbook(decrypted_file_path)

    sheet = workbook.active

    income_column = 'A'
    tax_rate_column = 'B'
    results_column = 'C'

    print(f"Value in {income_column}2:", sheet[f'{income_column}2'].value)

    # Assuming income is in column A and tax rate in column B
    sheet[f'{results_column}1'] = 'Годовые налоги'

    for row in range(2, sheet.max_row + 1):
        income_cell = sheet[f'{income_column}{row}'].value
        tax_rate_cell = sheet[f'{tax_rate_column}{row}'].value

        try:
            income = float(income_cell)
            tax_rate = float(tax_rate_cell)
            annual_tax = income * (tax_rate / 100.0)
            sheet[f'{results_column}{row}'].value = annual_tax
        except ValueError:
            print(f"Error processing row {row}: income={income_cell}, tax_rate={tax_rate_cell}")
            sheet[f'{results_column}{row}'].value = 'Error in data'

    # Optionally save the processed workbook if needed
    processed_file_path = file_path.replace('.xlsx', '_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)

    return encrypted_file_path

def process_net_income(file_path):
    key = get_key_from_file('Чистый доход_enc.xlsx')  # Make sure the filename matches your actual use case
    decrypted_file_path = decrypt_file(file_path, key)  # Decrypt the file
    workbook = load_workbook(decrypted_file_path)
    sheet = workbook.active

    # Define the columns
    gross_income_column = 'A'
    total_expenses_column = 'B'
    annual_taxes_column = 'C'
    net_income_column = 'D'

    # Header for net income
    sheet[f'{net_income_column}1'] = 'Чистый доход'

    # Calculate net income and store it in column D
    for row in range(2, sheet.max_row + 1):
        gross_income_cell = sheet[f'{gross_income_column}{row}'].value
        total_expenses_cell = sheet[f'{total_expenses_column}{row}'].value
        annual_taxes_cell = sheet[f'{annual_taxes_column}{row}'].value

        try:
            gross_income = float(gross_income_cell)
            total_expenses = float(total_expenses_cell)
            annual_taxes = float(annual_taxes_cell)
            net_income = gross_income - total_expenses - annual_taxes
            sheet[f'{net_income_column}{row}'].value = net_income
        except ValueError:
            print(f"Error processing row {row}: gross_income={gross_income_cell}, total_expenses={total_expenses_cell}, annual_taxes={annual_taxes_cell}")
            sheet[f'{net_income_column}{row}'].value = 'Error in data'

    # Save the processed data
    processed_file_path = file_path.replace('.xlsx', '_net_income_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)  # Re-encrypt the processed file

    return encrypted_file_path

def process_investment_income_percentage(file_path):
    key = get_key_from_file('Процент дохода от инвестиций_enc.xlsx')  # Ensure the key retrieval matches the encrypted filename
    decrypted_file_path = decrypt_file(file_path, key)  # Decrypt the file
    workbook = load_workbook(decrypted_file_path)
    sheet = workbook.active

    # Define the columns
    net_profit_from_investments_column = 'A'
    investment_cost_column = 'B'
    investment_income_percentage_column = 'C'

    # Header for investment income percentage
    sheet[f'{investment_income_percentage_column}1'] = 'Процент дохода от инвестиций'

    # Calculate investment income percentage and store it in column C
    for row in range(2, sheet.max_row + 1):
        net_profit_cell = sheet[f'{net_profit_from_investments_column}{row}'].value
        investment_cost_cell = sheet[f'{investment_cost_column}{row}'].value

        try:
            net_profit = float(net_profit_cell)
            investment_cost = float(investment_cost_cell)
            if investment_cost != 0:  # Prevent division by zero
                investment_income_percentage = (net_profit / investment_cost) * 100
                sheet[f'{investment_income_percentage_column}{row}'].value = investment_income_percentage
            else:
                sheet[f'{investment_income_percentage_column}{row}'].value = 'Error: Division by Zero'
        except ValueError:
            print(f"Error processing row {row}: net_profit={net_profit_cell}, investment_cost={investment_cost_cell}")
            sheet[f'{investment_income_percentage_column}{row}'].value = 'Error in data'

    # Save the processed data
    processed_file_path = file_path.replace('.xlsx', '_investment_income_percentage_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)  # Re-encrypt the processed file

    return encrypted_file_path

def process_depreciation_calculation(file_path):
    key = get_key_from_file('Расчет амортизации_enc.xlsx')  # Adjust filename if different
    decrypted_file_path = decrypt_file(file_path, key)  # Decrypt the file
    workbook = load_workbook(decrypted_file_path)
    sheet = workbook.active

    # Define the columns
    initial_asset_cost_column = 'A'
    residual_value_column = 'B'
    asset_lifespan_column = 'C'
    depreciation_column = 'D'

    # Header for depreciation
    sheet[f'{depreciation_column}1'] = 'Амортизация'

    # Calculate depreciation and store it in column D
    for row in range(2, sheet.max_row + 1):
        initial_cost_cell = sheet[f'{initial_asset_cost_column}{row}'].value
        residual_value_cell = sheet[f'{residual_value_column}{row}'].value
        asset_lifespan_cell = sheet[f'{asset_lifespan_column}{row}'].value

        try:
            initial_cost = float(initial_cost_cell)
            residual_value = float(residual_value_cell)
            asset_lifespan = float(asset_lifespan_cell)
            if asset_lifespan != 0:  # Prevent division by zero
                depreciation = (initial_cost - residual_value) / asset_lifespan
                sheet[f'{depreciation_column}{row}'].value = depreciation
            else:
                sheet[f'{depreciation_column}{row}'].value = 'Error: Division by Zero'
        except ValueError:
            print(f"Error processing row {row}: initial_cost={initial_cost_cell}, residual_value={residual_value_cell}, asset_lifespan={asset_lifespan_cell}")
            sheet[f'{depreciation_column}{row}'].value = 'Error in data'

    # Save the processed data
    processed_file_path = file_path.replace('.xlsx', '_depreciation_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)  # Re-encrypt the processed file

    return encrypted_file_path

def process_coverage_ratio(file_path):
    key = get_key_from_file('Коэффициент покрытия_enc.xlsx')  # Adjust filename if different
    decrypted_file_path = decrypt_file(file_path, key)  # Decrypt the file
    workbook = load_workbook(decrypted_file_path)
    sheet = workbook.active

    # Define the columns
    ebitda_column = 'A'
    total_interest_payments_column = 'B'
    coverage_ratio_column = 'C'

    # Header for coverage ratio
    sheet[f'{coverage_ratio_column}1'] = 'Коэффициент покрытия'

    # Calculate coverage ratio and store it in column C
    for row in range(2, sheet.max_row + 1):
        ebitda_cell = sheet[f'{ebitda_column}{row}'].value
        interest_payments_cell = sheet[f'{total_interest_payments_column}{row}'].value

        try:
            ebitda = float(ebitda_cell)
            interest_payments = float(interest_payments_cell)
            if interest_payments != 0:  # Prevent division by zero
                coverage_ratio = ebitda / interest_payments
                sheet[f'{coverage_ratio_column}{row}'].value = coverage_ratio
            else:
                sheet[f'{coverage_ratio_column}{row}'].value = 'Error: Division by Zero'
        except ValueError:
            print(f"Error processing row {row}: EBITDA={ebitda_cell}, interest_payments={interest_payments_cell}")
            sheet[f'{coverage_ratio_column}{row}'].value = 'Error in data'

    # Save the processed data
    processed_file_path = file_path.replace('.xlsx', '_coverage_ratio_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)  # Re-encrypt the processed file

    return encrypted_file_path
def process_solvency_ratio(file_path):
    key = get_key_from_file('Коэффициент солвентности_enc.xlsx')  # Ensure the key retrieval matches the encrypted filename
    decrypted_file_path = decrypt_file(file_path, key)  # Decrypt the file
    workbook = load_workbook(decrypted_file_path)
    sheet = workbook.active

    # Define the columns
    assets_column = 'A'
    liabilities_column = 'B'
    solvency_ratio_column = 'C'

    # Header for solvency ratio
    sheet[f'{solvency_ratio_column}1'] = 'Коэффициент солвентности'

    # Calculate solvency ratio and store it in column C
    for row in range(2, sheet.max_row + 1):
        assets_cell = sheet[f'{assets_column}{row}'].value
        liabilities_cell = sheet[f'{liabilities_column}{row}'].value

        try:
            assets = float(assets_cell)
            liabilities = float(liabilities_cell)
            if liabilities != 0:  # Prevent division by zero
                solvency_ratio = assets / liabilities
                sheet[f'{solvency_ratio_column}{row}'].value = solvency_ratio
            else:
                sheet[f'{solvency_ratio_column}{row}'].value = 'Error: Division by Zero'
        except ValueError:
            print(f"Error processing row {row}: assets={assets_cell}, liabilities={liabilities_cell}")
            sheet[f'{solvency_ratio_column}{row}'].value = 'Error in data'

    # Save the processed data
    processed_file_path = file_path.replace('.xlsx', '_solvency_ratio_processed.xlsx')
    workbook.save(processed_file_path)
    encrypted_file_path = encrypt_file(processed_file_path, key)  # Re-encrypt the processed file

    return encrypted_file_path


def get_key_from_file(filename):
    with open(os.path.join(app.config['UPLOAD_FOLDER'], 'keys.txt'), 'r') as file:
        lines = file.readlines()  # Read all lines into a list
        lines.reverse()  # Reverse the list to check from the bottom up

        for line in lines:
            key, saved_filename = line.strip().split(':')
            if saved_filename == filename:
                return key
    return None


def decrypt_file(file_path, key):
    workbook = load_workbook(file_path)
    sheet = workbook.active

    decrypted_file_path = file_path.replace('_enc', '_dec')
    decrypted_workbook = Workbook()
    decrypted_sheet = decrypted_workbook.active

    key_bytes = hashlib.sha256(key.encode()).digest()  # Use the same SHA256 hash for the key

    for row_idx, row in enumerate(sheet.iter_rows(), start=1):
        for col_idx, cell in enumerate(row, start=1):
            if cell.value is not None:
                decrypted_value = decrypt_value(cell.value, key_bytes)
                decrypted_sheet.cell(row=row_idx, column=col_idx, value=decrypted_value)

    decrypted_workbook.save(decrypted_file_path)
    return decrypted_file_path


def decrypt_value(encrypted_value, key_bytes):
    data = b64decode(encrypted_value)
    iv = data[:16]
    encrypted_data = data[16:]
    cipher = Cipher(algorithms.AES(key_bytes), modes.CFB(iv), backend=default_backend())
    decryptor = cipher.decryptor()
    decrypted_data = decryptor.update(encrypted_data) + decryptor.finalize()
    decrypted_value = decrypted_data.decode('utf-8')
    return decrypted_value
